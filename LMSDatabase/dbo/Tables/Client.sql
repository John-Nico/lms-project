﻿CREATE TABLE [dbo].[Client] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [ClientName]      NVARCHAR (150) NOT NULL,
    [VendorNumber]    NVARCHAR (50)  NOT NULL,
    [VATNumber]       VARCHAR (30)   NOT NULL,
    [InvoiceAddress]  NVARCHAR (MAX) NOT NULL,
    [PhysicalAddress] NVARCHAR (MAX) NOT NULL,
    [PostalCode]      VARCHAR (10)   NOT NULL,
    CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED ([Id] ASC)
);

