﻿CREATE TABLE [dbo].[Department] (
    [Id]                 INT            IDENTITY (1, 1) NOT NULL,
    [ClientId]           INT            NOT NULL,
    [ContactPersonName]  NVARCHAR (100) NOT NULL,
    [Designation]        NVARCHAR (100) NOT NULL,
    [ContactPersonEmail] VARCHAR (50)   NOT NULL,
    [ContactPersonTel]   NVARCHAR (50)  NOT NULL,
    CONSTRAINT [PK_Department] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Department_Client] FOREIGN KEY ([ClientId]) REFERENCES [dbo].[Client] ([Id])
);

