﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class PrivateController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Planning
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> PrivateList()
        {
            var Privates = await db.PrivateContract.ToListAsync();
            return View(Privates);
        }

        [HttpGet]
        public ActionResult AddPrivate()
        {
            ViewBag.Clients = db.Clients;
            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(PrivateContract Privatecontract)
        {

            Privatecontract.PrivateContractDescription = Privatecontract.TypeOfTraining + Privatecontract.PrivateCourseName + Privatecontract.CourseDuration;
            Privatecontract.LastUpdateDate = DateTime.Now.ToString();

            /*Temp --Remove*/
            Privatecontract.ContractExpireDate = DateTime.Now.ToString();

            db.PrivateContract.Add(Privatecontract);
            
            await db.SaveChangesAsync();            

            return RedirectToAction("PrivateList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewPrivate(int id)
        {
            PrivateModel Private = new PrivateModel();

            Private.PrivateContract = await db.PrivateContract.Where(c => c.Id == id).SingleAsync();
            Private.Client = await db.Clients.Where(c => c.Id == Private.PrivateContract.ClientId).SingleAsync();
            
            return View(Private);
        }

        [HttpPost]
        public async Task<ActionResult> UpdatePrivate(PrivateModel model)
        {
      
            var Private =  db.PrivateContract.Find(model.PrivateContract.Id);

            Private.ClientId = model.PrivateContract.ClientId;
            Private.PrivateName = model.PrivateContract.PrivateName;
            Private.TypeOfTraining = model.PrivateContract.TypeOfTraining;
            Private.CourseDuration = model.PrivateContract.CourseDuration;
            Private.SAQAUnitStandardID = model.PrivateContract.SAQAUnitStandardID;
            Private.NonUnitStandard = model.PrivateContract.NonUnitStandard;
            Private.PrivateContractDescription = model.PrivateContract.TypeOfTraining + model.PrivateContract.PrivateCourseName + model.PrivateContract.CourseDuration;
            Private.ContractExpireDate = model.PrivateContract.ContractExpireDate;
            Private.PriceExclVat = model.PrivateContract.PriceExclVat;
            Private.LastUpdateDate = DateTime.Now.ToString();

            db.SaveChanges();

            return RedirectToAction("PrivateList");
        }

        [HttpGet]
        public async Task<ActionResult> EditPrivate(Planning list)
        {
            PrivateModel Private = new PrivateModel();

            Private.PrivateContract = await db.PrivateContract.Where(c => c.Id == list.Id).SingleAsync();

            ViewBag.Clients = db.Clients;
            ViewBag.ClientsId = new SelectList(db.Clients, "Id", "ClientName", Private.PrivateContract.ClientId);

            return View(Private);
        }
    }
}