﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class PlanningController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Planning
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> PlanningList()
        {
            var planning = await db.Plannings.ToListAsync();
            return View(planning);
        }

        [HttpGet]
        public ActionResult AddPlanning()
        {

            ViewBag.Department = db.Departments;
            ViewBag.RFQ = db.RFQContract;
            ViewBag.Tender = db.TenderContract;
            ViewBag.Private = db.PrivateContract;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Planning planning)
        {
            planning.Status = "Pending";
            planning.LastUpdateDate = DateTime.Now.ToString();
            db.Plannings.Add(planning);
            
            await db.SaveChangesAsync();            

            return RedirectToAction("PlanningList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewPlanning(int id)
        {
            PlanningModel planning = new PlanningModel();

            planning.Planning = await db.Plannings.Where(c => c.Id == id).SingleAsync();
            planning.Departments = await db.Departments.Where(d => d.Id == planning.Planning.DepartmentId).SingleAsync();
            planning.Courses = await db.Courses.Where(c => c.Id == planning.Planning.CourseId).SingleAsync();

            return View(planning);
        }

        [HttpPost]
        public async Task<ActionResult> UpdatePlanning(PlanningModel model)
        {
      
            var planning =  db.Plannings.Find(model.Planning.Id);

            planning.DepartmentId = model.Planning.DepartmentId;
            planning.CourseId = model.Planning.CourseId;
            planning.CourseType = model.Planning.CourseType;
            planning.NumberOfLearners = model.Planning.NumberOfLearners;
            planning.PlanningName = model.Planning.PlanningName;
            planning.Dates = model.Planning.Dates;
            planning.VenueLocation = model.Planning.VenueLocation;
            planning.GroupSize = model.Planning.GroupSize;
            planning.Status = model.Planning.Status;
            planning.LastUpdateDate = DateTime.Now.ToString();

            db.SaveChanges();

            return RedirectToAction("PlanningList");
        }

        [HttpGet]
        public async Task<ActionResult> EditPlanning(Planning list)
        {
            PlanningModel planning = new PlanningModel();

            planning.Planning = await db.Plannings.Where(c => c.Id == list.Id).SingleAsync();
            planning.Departments = await db.Departments.Where(d => d.Id == planning.Planning.DepartmentId).SingleAsync();
            planning.Courses = await db.Courses.Where(c => c.Id == planning.Planning.CourseId).SingleAsync();

            ViewBag.Department = db.Departments;
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", planning.Planning.DepartmentId);
            ViewBag.Course = db.Courses;
            ViewBag.CourseId = new SelectList(db.Courses, "Id", "CourseName", planning.Planning.CourseId);
            ViewBag.RFQ = db.RFQContract;
            ViewBag.Tender = db.TenderContract;
            ViewBag.Private = db.PrivateContract;

            return View(planning);
        }
    }
}