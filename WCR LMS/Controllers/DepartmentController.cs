﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class DepartmentController : Controller
    {

        private LMSDb db = new LMSDb();

        [HttpGet]
        public async Task<ActionResult> DepartmentList()
        {
            var department = await db.Departments.ToListAsync();
            return View(department);
        }

        [HttpGet]
        public ActionResult AddDepartment()
        {
            ViewBag.Clients = db.Clients; 
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Department department)
        {
            
            db.Departments.Add(department);
            
            await db.SaveChangesAsync();

            return RedirectToAction("DepartmentList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewDepartment(int id)
        {            
            DepartmentViewModel model = new DepartmentViewModel();

            ViewBag.Clients = db.Clients;
            model.Department = await db.Departments.Where(dep => dep.Id == id).SingleAsync();
            model.Client = await db.Clients.Where(client => client.Id == model.Department.ClientId).SingleAsync();
            model.Learners = await db.Learners.Where(learner => learner.DepartmentId == model.Department.Id).ToListAsync();

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateDepartment(DepartmentViewModel model)
        { 
            var department = db.Departments.Find(model.Department.Id);

            department.ClientId = model.Department.ClientId;
            department.DepartmentName = model.Department.DepartmentName;
            department.ContactPersonName = model.Department.ContactPersonName;
            department.Designation = model.Department.Designation;
            department.ContactPersonEmail = model.Department.ContactPersonEmail;
            department.ContactPersonTel1 = model.Department.ContactPersonTel1;
            department.ContactPersonTel2 = model.Department.ContactPersonTel2;
            department.ContactPersonFax = model.Department.ContactPersonFax;

            db.SaveChanges();

            return RedirectToAction("DepartmentList");
        }

        [HttpGet]
        public async Task<ActionResult> EditDepartment(Department list)
        {
            DepartmentViewModel model = new DepartmentViewModel();
            
            model.Department = await db.Departments.Where(c => c.Id == list.Id).SingleAsync();
            model.Client = await db.Clients.Where(client => client.Id == model.Department.ClientId).SingleAsync();

            ViewBag.Clients = db.Clients;
            ViewBag.ClientsId = new SelectList(db.Clients, "Id", "ClientName", model.Department.ClientId);
          
            return View(model);
        }
    }
}