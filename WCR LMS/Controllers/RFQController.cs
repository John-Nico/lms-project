﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class RFQController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Planning
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> RFQList()
        {
            var RFQs = await db.RFQContract.ToListAsync();
            return View(RFQs);
        }

        [HttpGet]
        public ActionResult AddRFQ()
        {
            ViewBag.Clients = db.Clients;
            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(RFQContract RFQcontract)
        {

            RFQcontract.RFQContractDescription = RFQcontract.TypeOfTraining + RFQcontract.RFQCourseName + RFQcontract.CourseDuration;
            RFQcontract.LastUpdateDate = DateTime.Now.ToString();

            db.RFQContract.Add(RFQcontract);
            
            await db.SaveChangesAsync();            

            return RedirectToAction("RFQList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewRFQ(int id)
        {
            RFQModel RFQ = new RFQModel();

            RFQ.RFQContract = await db.RFQContract.Where(c => c.Id == id).SingleAsync();
            RFQ.Client = await db.Clients.Where(c => c.Id == RFQ.RFQContract.ClientId).SingleAsync();
            
            return View(RFQ);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateRFQ(RFQModel model)
        {
      
            var RFQ =  db.RFQContract.Find(model.RFQContract.Id);

            RFQ.ClientId = model.RFQContract.ClientId;
            RFQ.RFQName = model.RFQContract.RFQName;
            RFQ.RFQNumber = model.RFQContract.RFQNumber;
            RFQ.TypeOfTraining = model.RFQContract.TypeOfTraining;
            RFQ.CourseDuration = model.RFQContract.CourseDuration;
            RFQ.SAQAUnitStandardID = model.RFQContract.SAQAUnitStandardID;
            RFQ.NonUnitStandard = model.RFQContract.NonUnitStandard;
            RFQ.RFQContractDescription = model.RFQContract.TypeOfTraining + model.RFQContract.RFQCourseName + model.RFQContract.CourseDuration;
            RFQ.ContractExpireDate = model.RFQContract.ContractExpireDate;
            RFQ.PriceExclVat = model.RFQContract.PriceExclVat;
            RFQ.LastUpdateDate = DateTime.Now.ToString();

            db.SaveChanges();

            return RedirectToAction("RFQList");
        }

        [HttpGet]
        public async Task<ActionResult> EditRFQ(Planning list)
        {
            RFQModel RFQ = new RFQModel();

            RFQ.RFQContract = await db.RFQContract.Where(c => c.Id == list.Id).SingleAsync();

            ViewBag.Clients = db.Clients;
            ViewBag.ClientsId = new SelectList(db.Clients, "Id", "ClientName", RFQ.RFQContract.ClientId);

            return View(RFQ);
        }
    }
}