﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class ClientController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Client
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ClientList()
        {
            var clients = await db.Clients.ToListAsync();
            return View(clients);
        }

        [HttpGet]
        public ActionResult AddClient()
        {           
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Client client)
        {
            db.Clients.Add(client);
            await db.SaveChangesAsync();            

            return RedirectToAction("ClientList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewClient(int id)
        {
            ClientViewModel client = new ClientViewModel();

            client.Client = await db.Clients.Where(c => c.Id == id).SingleAsync();
            client.Departments = await db.Departments.Where(d => d.ClientId == id).ToListAsync();
            
            return View(client);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateClient(ClientViewModel model)
        {
      

            var clients =  db.Clients.Find(model.Client.Id);

            clients.ClientName = model.Client.ClientName;
            clients.VendorNumber = model.Client.VendorNumber;
            clients.VATNumber = model.Client.VATNumber;
            clients.InvoiceAddress = model.Client.InvoiceAddress;
            clients.PhysicalAddress = model.Client.PhysicalAddress;
            clients.PostalCode = model.Client.PostalCode;

            db.SaveChanges();

            return RedirectToAction("ClientList");
        }

        [HttpGet]
        public async Task<ActionResult> EditClient(Client list)
        {
            ClientViewModel client = new ClientViewModel();

            client.Client = await db.Clients.Where(c => c.Id == list.Id).SingleAsync();

            return View(client);
        }
    }
}