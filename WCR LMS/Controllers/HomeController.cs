﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using WCR_LMS.Models;

namespace MVCEventCalendar.Controllers
{
    public class HomeController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
    }
}