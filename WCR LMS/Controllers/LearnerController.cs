﻿using Common;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WCR_LMS.DataContexts;
using WCR_LMS.Models;
using Microsoft.AspNet.Identity;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class LearnerController : Controller
    {
        private LMSDb db = new LMSDb();

        [HttpGet]
        public async Task<ActionResult> LearnerList()
        {
            var learner = await db.Learners.ToListAsync();
            return View(learner);
        }

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set { _roleManager = value; }
        }

        // GET: Learner
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddLearner()
        {
            ViewBag.Clients = db.Clients;
            ViewBag.Departments = db.Departments;
            ViewBag.AlternateType = db.AlternateIDTypes;
            ViewBag.Titles = db.Titles;
            ViewBag.Gender = db.Genders;
            ViewBag.Equity = db.Equities;
            ViewBag.Disability = db.Disabilities;
            ViewBag.Language = db.Languages;
            ViewBag.ResidentialStatus = db.ResidentialStatusses;
            ViewBag.SocialEconomicStatus = db.SocioEconomicStatusses;
            ViewBag.Province = db.Provinces;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(Learner learner)
        {
            //Create user and emails them
            /*var user = new ApplicationUser { UserName = learner.Email, Email = learner.Email };

            string password = System.Web.Security.Membership.GeneratePassword(6, 0);
            var result = await UserManager.CreateAsync(user, password);

            await UserManager.AddToRoleAsync(user.Id, "Learner");

            learner.AspNetUserId = user.Id;

            SmtpClient smtpServer = new SmtpClient("smtp.live.com");
            var mail = new MailMessage();

            mail.From = new MailAddress("john-nico@live.co.za");
            mail.To.Add("john-nico@live.co.za");
            mail.To.Add(learner.Email);
            mail.Subject = "Learner Registration Successful " + learner.FirstName + " " + learner.LastName;
            mail.IsBodyHtml = true;

            string htmlBody;
            htmlBody = "This is the mail body<br/>Your username: " + user.Email + "<br/> password: " + password;
            mail.Body = htmlBody;
            smtpServer.Port = 587;
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Credentials = new System.Net.NetworkCredential("john-nico@live.co.za", "nico-john");
            smtpServer.EnableSsl = true;
            smtpServer.Send(mail);*/

            //var user = new ApplicationUser { UserName = learner.Email, Email = learner.Email };

            //string password = System.Web.Security.Membership.GeneratePassword(6, 0);
            //var result = await UserManager.CreateAsync(user, password);

            //await UserManager.AddToRoleAsync(user.Id, "Learner");

            //learner.AspNetUserId = user.Id;

            db.Learners.Add(learner);
            await db.SaveChangesAsync();

            return RedirectToAction("LearnerList");
        }

        [HttpPost]
        public async Task<ActionResult> UpdateLearner(LearnerViewModel model)
        {
            var learner = db.Learners.Find(model.Learner.Id);

            learner.DepartmentId = model.Learner.DepartmentId;
            learner.LearnerNumber = model.Learner.LearnerNumber;
            learner.IDNumber = model.Learner.IDNumber;
            learner.AlternateIDTypeId = model.Learner.AlternateIDTypeId;
            learner.Initials = model.Learner.Initials;
            learner.TitleId = model.Learner.TitleId;
            learner.FirstName = model.Learner.FirstName;
            learner.LastName = model.Learner.LastName;
            learner.DateOfBirth = model.Learner.DateOfBirth;
            learner.GenderId = model.Learner.GenderId;
            learner.EquityId = model.Learner.EquityId;
            learner.DisabilityId = model.Learner.DisabilityId;
            learner.LanguageId = model.Learner.LanguageId;
            learner.ResidentialStatusId = model.Learner.ResidentialStatusId;
            learner.SociaEconomicStatusId = model.Learner.SociaEconomicStatusId;
            learner.TelNumber = model.Learner.TelNumber;
            learner.CellNumber = model.Learner.CellNumber;
            learner.FaxNumber = model.Learner.FaxNumber;
            learner.Email = model.Learner.Email;
            learner.PhysicalAddress1 = model.Learner.PhysicalAddress1;
            learner.PhysicalAddress2 = model.Learner.PhysicalAddress2;
            learner.PhysicalAddress3 = model.Learner.PhysicalAddress3;
            learner.PhysicalPostalCode = model.Learner.PhysicalPostalCode;
            learner.PhysicalProvinceId = model.Learner.PhysicalProvinceId;
            learner.PostalAddress1 = model.Learner.PostalAddress1;
            learner.PostalAddress2 = model.Learner.PostalAddress2;
            learner.PostalAddress3 = model.Learner.PostalAddress3;
            learner.PostalCode = model.Learner.PostalCode;
            learner.PostalProvinceId = model.Learner.PostalProvinceId;
            learner.EmployerSDLNumber = model.Learner.EmployerSDLNumber;

            db.SaveChanges();

            return RedirectToAction("LearnerList");
        }

        [HttpGet]
        public async Task<ActionResult> EditLearner(Learner list)
        {
            LearnerViewModel model = new LearnerViewModel();

            model.Learner = await db.Learners.Where(l => l.Id == list.Id).SingleAsync();
            model.Department = await db.Departments.Where(d => d.Id == model.Learner.DepartmentId).SingleAsync();
            model.Client = await db.Clients.Where(c => c.Id == model.Department.ClientId).SingleAsync();

            ViewBag.Clients = db.Clients;
            ViewBag.Departments = db.Departments;
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", model.Learner.DepartmentId);
            ViewBag.AlternateType = db.AlternateIDTypes;
            ViewBag.AlternateTypeId = new SelectList(db.AlternateIDTypes, "Id", "Description", model.Learner.AlternateIDTypeId);
            ViewBag.Titles = db.Titles;
            ViewBag.Gender = db.Genders;
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Description", model.Learner.GenderId);
            ViewBag.Equity = db.Equities;
            ViewBag.EquityId = new SelectList(db.Equities, "Id", "Description", model.Learner.EquityId);
            ViewBag.Disability = db.Disabilities;
            ViewBag.DisabilityId = new SelectList(db.Disabilities, "Id", "Description", model.Learner.DisabilityId);
            ViewBag.Language = db.Languages;
            ViewBag.LanguageId = new SelectList(db.Languages, "Id", "Description", model.Learner.LanguageId);
            ViewBag.ResidentialStatus = db.ResidentialStatusses;
            ViewBag.ResidentialStatusId = new SelectList(db.ResidentialStatusses, "Id", "Description", model.Learner.ResidentialStatusId);
            ViewBag.SocialEconomicStatus = db.SocioEconomicStatusses;
            ViewBag.SocialEconomicStatusId = new SelectList(db.SocioEconomicStatusses, "Id", "Description", model.Learner.SociaEconomicStatusId);
            ViewBag.Provinces = db.Provinces;
            ViewBag.PhysicalProvinceId = new SelectList(db.Provinces, "Id", "Description", model.Learner.PhysicalProvinceId);
            ViewBag.PostalProvinceId = new SelectList(db.Provinces, "Id", "Description", model.Learner.PostalProvinceId);

            return View(model);
        }

        public async Task<ActionResult> ViewLearner(int id)
        {
            LearnerViewModel model = new LearnerViewModel();

            var learner = from l in db.Learners
                          join d in db.Departments on l.DepartmentId equals d.Id
                          where l.Id == id
                          select new { learner = l, departmentName = d.DepartmentName};

            model.AlternateIDTypes = await db.AlternateIDTypes.ToListAsync();
            model.Disabilities = await db.Disabilities.ToListAsync();
            model.Equities = await db.Equities.ToListAsync();
            model.Genders = await db.Genders.ToListAsync();
            model.Languages = await db.Languages.ToListAsync();
            model.Provinces = await db.Provinces.ToListAsync();
            model.Titles = await db.Titles.ToListAsync();
            model.SocioEconomicStatusses = await db.SocioEconomicStatusses.ToListAsync();
            model.ResidentialStatusses = await db.ResidentialStatusses.ToListAsync();

            model.Learner = learner.First().learner;
            //model.ClientName = learner.First().clientName;
            model.DepartmentName = learner.First().departmentName;

            //model.Learner = await db.Learners.Where(x => x.Id == id).SingleAsync();

            //var client = await db.Clients.Where(x => x.Id == model.Learner.ClientId).SingleAsync();
            //var department = await db.Departments.Where(x => x.Id == model.Learner.DepartmentId).SingleAsync();

            //model.ClientName = client.ClientName;
            //model.DepartmentName = department.DepartmentName;

            return View(model);

        }

        public async Task<ActionResult> ViewLearnerProfile(int id)
        {
            LearnerViewModel model = new LearnerViewModel();
            string userId = User.Identity.GetUserId();

            model.Learner = await db.Learners.Where(x => x.AspNetUserId == userId).SingleAsync();

            model.AlternateIDTypes = await db.AlternateIDTypes.ToListAsync();
            model.Disabilities = await db.Disabilities.ToListAsync();
            model.Equities = await db.Equities.ToListAsync();
            model.Genders = await db.Genders.ToListAsync();
            model.Languages = await db.Languages.ToListAsync();
            model.Provinces = await db.Provinces.ToListAsync();
            model.Titles = await db.Titles.ToListAsync();
            model.SocioEconomicStatusses = await db.SocioEconomicStatusses.ToListAsync();
            model.ResidentialStatusses = await db.ResidentialStatusses.ToListAsync();

            return View(model);
        }

        public async Task<ActionResult> ViewProfile()
        {
            LearnerViewModel model = new LearnerViewModel();
            string userId = User.Identity.GetUserId();

            model.Learner = await db.Learners.Where(x => x.AspNetUserId == userId).SingleAsync();

            model.AlternateIDTypes = await db.AlternateIDTypes.ToListAsync();
            model.Disabilities = await db.Disabilities.ToListAsync();
            model.Equities = await db.Equities.ToListAsync();
            model.Genders = await db.Genders.ToListAsync();
            model.Languages = await db.Languages.ToListAsync();
            model.Provinces = await db.Provinces.ToListAsync();
            model.Titles = await db.Titles.ToListAsync();
            model.SocioEconomicStatusses = await db.SocioEconomicStatusses.ToListAsync();
            model.ResidentialStatusses = await db.ResidentialStatusses.ToListAsync();

            return View(model);
        }
    }
}