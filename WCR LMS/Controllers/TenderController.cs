﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class TenderController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: Planning
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> TenderList()
        {
            var tenders = await db.TenderContract.ToListAsync();
            return View(tenders);
        }

        [HttpGet]
        public ActionResult AddTender()
        {
            ViewBag.Clients = db.Clients;
            
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(TenderContract tendercontract)
        {

            tendercontract.TenderContractDescription = tendercontract.TypeOfTraining + tendercontract.TenderCourseName + tendercontract.CourseDuration;
            tendercontract.LastUpdateDate = DateTime.Now.ToString();

            /*Temp --Remove*/
            tendercontract.ContractExpireDate = DateTime.Now.ToString();

            if (tendercontract.NonUnitStandard == null)
            { tendercontract.NonUnitStandard = 0; }
            else { tendercontract.SAQAUnitStandardID = 0; }

            db.TenderContract.Add(tendercontract);
            
            await db.SaveChangesAsync();            

            return RedirectToAction("TenderList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewTender(int id)
        {
            TenderModel tender = new TenderModel();

            tender.TenderContract = await db.TenderContract.Where(c => c.Id == id).SingleAsync();
            tender.Client = await db.Clients.Where(c => c.Id == tender.TenderContract.ClientId).SingleAsync();
            
            return View(tender);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateTender(TenderModel model)
        {
      
            var tender =  db.TenderContract.Find(model.TenderContract.Id);

            tender.ClientId = model.TenderContract.ClientId;
            tender.TenderName = model.TenderContract.TenderName;
            tender.ContractNumber = model.TenderContract.ContractNumber;
            tender.TypeOfTraining = model.TenderContract.TypeOfTraining;
            tender.CourseDuration = model.TenderContract.CourseDuration;
            tender.SAQAUnitStandardID = model.TenderContract.SAQAUnitStandardID;
            tender.NonUnitStandard = model.TenderContract.NonUnitStandard;
            tender.TenderContractDescription = model.TenderContract.TypeOfTraining + model.TenderContract.TenderCourseName + model.TenderContract.CourseDuration;
            tender.ContractExpireDate = model.TenderContract.ContractExpireDate;
            tender.PriceExclVat = model.TenderContract.PriceExclVat;
            tender.LastUpdateDate = DateTime.Now.ToString();

            db.SaveChanges();

            return RedirectToAction("TenderList");
        }

        [HttpGet]
        public async Task<ActionResult> EditTender(Planning list)
        {
            TenderModel tender = new TenderModel();

            tender.TenderContract = await db.TenderContract.Where(c => c.Id == list.Id).SingleAsync();
            //client.Departments = await db.Departments.Where(d => d.ClientId == id).ToListAsync();

            ViewBag.Clients = db.Clients;
            ViewBag.ClientsId = new SelectList(db.Clients, "Id", "ClientName", tender.TenderContract.ClientId);

            return View(tender);
        }
    }
}