﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WCR_LMS.DataContexts;
using System.Data;
using System.Data.Entity;
using Common;
using WCR_LMS.Models;

namespace WCR_LMS.Controllers
{
    [Authorize]
    public class ProvisionalDateController : Controller
    {

        private LMSDb db = new LMSDb();

        // GET: ProvisionalDate
        public ActionResult ProvisionalDateCalendar()
        {
            return View();
        }

        [HttpGet]
        public async Task<ActionResult> ProvisionalDateList()
        {
            var provisionaldate = await db.ProvisionalDates.ToListAsync();
            return View(provisionaldate);
        }

        [HttpGet]
        public ActionResult AddProvisionalDate()
        {
            ViewBag.Planning = db.Plannings.Where(p => p.Status == "Pending");

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(ProvisionalDate provisionaldate)
        {
            provisionaldate.Status = "Pending";
            provisionaldate.LastUpdateDate = DateTime.Now.ToString();
            db.ProvisionalDates.Add(provisionaldate);
            
            await db.SaveChangesAsync();            

            return RedirectToAction("ProvisionalDateList");
        }

        [HttpGet]
        public async Task<ActionResult> ViewProvisionalDate(int id)
        {
            ProvisionalDateModel provisionaldate = new ProvisionalDateModel();
            
            provisionaldate.ProvisionalDate = await db.ProvisionalDates.Where(c => c.Id == id).SingleAsync();
            provisionaldate.Planning = await db.Plannings.Where(d => d.Id == provisionaldate.ProvisionalDate.PlanningId).SingleAsync();

            return View(provisionaldate);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateProvisionalDate(ProvisionalDateModel model)
        {
      
            var provisionaldate =  db.ProvisionalDates.Find(model.ProvisionalDate.Id);

            provisionaldate.PlanningId = model.ProvisionalDate.PlanningId;
            provisionaldate.AssessorId = model.ProvisionalDate.AssessorId;
            provisionaldate.GroupNo = model.ProvisionalDate.GroupNo;
            provisionaldate.NumberOfLearners = model.ProvisionalDate.NumberOfLearners;
            provisionaldate.ProvisionalDateDescription = model.ProvisionalDate.ProvisionalDateDescription;
            provisionaldate.Dates = model.ProvisionalDate.Dates;
            provisionaldate.Colour = model.ProvisionalDate.Colour;
            provisionaldate.Status = model.ProvisionalDate.Status;
            provisionaldate.LastUpdateDate = DateTime.Now.ToString();

            db.SaveChanges();

            return RedirectToAction("ProvisionalDateList");
        }

        [HttpGet]
        public async Task<ActionResult> EditProvisionalDate(ProvisionalDate list)
        {
            ProvisionalDateModel provisionaldate = new ProvisionalDateModel();

            provisionaldate.ProvisionalDate = await db.ProvisionalDates.Where(c => c.Id == list.Id).SingleAsync();
            provisionaldate.Planning = await db.Plannings.Where(d => d.Id == provisionaldate.ProvisionalDate.PlanningId).SingleAsync();

            ViewBag.Planning = db.Plannings;
            ViewBag.PlanningId = new SelectList(db.Plannings, "Id", "PlanningName", provisionaldate.ProvisionalDate.PlanningId);

            return View(provisionaldate);
        }

        [HttpGet]
        public ActionResult DateView()
        {

            ViewBag.Planning = db.Plannings.Where(p => p.Status == "Pending");

            return View();
        }

        //Generate Calendar 
        public JsonResult GetEvents()
        {
            using (LMSDb dc = new LMSDb())
            {
                var events = dc.Events.ToList();
                return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        /*
        [HttpPost]
        public JsonResult SaveEvent(Events e)
        {
            var status = false;
            using (LMSDb dc = new LMSDb())
            {
                if (e.EventID > 0)
                {
                    //Update the event
                    var v = dc.Events.Where(a => a.EventID == e.EventID).FirstOrDefault();
                    if (v != null)
                    {
                        v.Subject = e.Subject;
                        v.Start = e.Start;
                        v.End = e.End;
                        v.Description = e.Description;
                        v.IsFullDay = e.IsFullDay;
                        v.ThemeColor = e.ThemeColor;
                    }
                }
                else
                {
                    dc.Events.Add(e);
                }

                dc.SaveChanges();
                status = true;

            }
            return new JsonResult { Data = new { status = status } };
        }

        [HttpPost]
        public JsonResult DeleteEvent(int eventID)
        {
            var status = false;
            using (LMSDb dc = new LMSDb())
            {
                var v = dc.Events.Where(a => a.EventID == eventID).FirstOrDefault();
                if (v != null)
                {
                    dc.Events.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
        */
    }
}