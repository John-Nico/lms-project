﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WCR_LMS.Startup))]
namespace WCR_LMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
