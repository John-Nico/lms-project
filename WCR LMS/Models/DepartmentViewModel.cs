﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCR_LMS.Models
{
    public class DepartmentViewModel
    {
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public Department Department { get; set; }
        public List<Learner> Learners { get; set; }

    }
}