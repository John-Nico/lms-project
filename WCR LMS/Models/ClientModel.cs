﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCR_LMS.Models
{
    public class ClientViewModel
    {
        public Client Client { get; set; }
        public List<Department> Departments { get; set; }

    }
}