﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCR_LMS.Models
{
    public class PrivateModel
    {
        public PrivateContract PrivateContract { get; set; }
        public Client Client { get; set; }

    }
}