﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCR_LMS.Models
{
    public class PlanningModel
    {
        public Planning Planning { get; set; }
        public Department Departments { get; set; }
        public CourseLookup Courses { get; set; }
    }
}