﻿using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WCR_LMS.Models
{
    public class LearnerViewModel
    {
        [DisplayName("Client Name")]
        public string ClientName { get; set; }
        [DisplayName("Department Name")]
        public string DepartmentName { get; set; }

        public int DepartmentId { get; set; }
        public Client Client { get; set; }
        public Department Department { get; set; }
        public Learner Learner { get; set; }

        //Lookups
        public List<TitleLookup> Titles { get; set; }
        public List<AlternateIDTypeLookup> AlternateIDTypes { get; set; }
        public List<GenderLookup> Genders { get; set; }
        public List<EquityLookup> Equities { get; set; }
        public List<DisabilityLookup> Disabilities { get; set; }
        public List<LanguageLookup> Languages { get; set; }
        public List<ResidentialStatusLookup> ResidentialStatusses { get; set; }
        public List<SocioEconomicStatusLookup> SocioEconomicStatusses { get; set; }
        public List<ProvinceLookup> Provinces { get; set; }


        #region Lookup Descriptions

        [DisplayName("Title")]
        public string TitleDescription
        {
            get
            {
                try
                {
                    return Titles.FirstOrDefault(x => x.Id == Learner.TitleId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Alternate ID Type")]
        public string AlternateIDTypeDescription
        {
            get
            {
                try
                {
                    return AlternateIDTypes.FirstOrDefault(x => x.Id == Learner.AlternateIDTypeId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Gender")]
        public string GenderDescription
        {
            get
            {
                try
                {
                    return Genders.FirstOrDefault(x => x.Id == Learner.GenderId).Description;
                }
                catch
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Equity")]
        public string EquityDescription
        {
            get
            {
                try
                {
                    return Equities.FirstOrDefault(x => x.Id == Learner.EquityId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Disability")]
        public string DisabilityDescription
        {
            get
            {
                try
                {
                    return Disabilities.FirstOrDefault(x => x.Id == Learner.DisabilityId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Language")]
        public string LanguageDescription
        {
            get
            {
                try
                {
                    return Languages.FirstOrDefault(x => x.Id == Learner.LanguageId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Residential Status")]
        public string ResidentialStatusDescription
        {
            get
            {
                try
                {
                    return ResidentialStatusses.FirstOrDefault(x => x.Id == Learner.ResidentialStatusId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        [DisplayName("Socio Economic Status")]
        public string SocioEconomicStatusDescription
        {
            get
            {
                try
                {
                    return SocioEconomicStatusses.FirstOrDefault(x => x.Id == Learner.SociaEconomicStatusId).Description;
                }
                catch 
                {
                    return "Not Specified";
                }
            }
        }

        public string PhysicalProvinceDescription
        {
            get
            {
                try
                {
                    return Provinces.FirstOrDefault(x => x.Id == Learner.PhysicalProvinceId).Description;
                }
                catch 
                {
                    return "";
                }
            }
        }

        public string PostalProvinceDescription
        {
            get
            {
                try
                {
                    return Provinces.FirstOrDefault(x => x.Id == Learner.PostalProvinceId).Description;
                }
                catch 
                {
                    return "";
                }
            }
        }

        #endregion

        public IEnumerable<SelectListItem> TitleSelect
        {
            get
            {
                return Titles.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.TitleId
                });
            }
        }

        public IEnumerable<SelectListItem> AlternateIDTypeSelect
        {
            get
            {
                return AlternateIDTypes.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.AlternateIDTypeId
                });
            }
        }

        public IEnumerable<SelectListItem> GenderSelect
        {
            get
            {
                return Genders.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.GenderId
                });
            }
        }

        public IEnumerable<SelectListItem> EquitySelect
        {
            get
            {
                return Equities.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.EquityId
                });
            }
        }

        public IEnumerable<SelectListItem> DisabilitySelect
        {
            get
            {
                return Disabilities.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.DisabilityId
                });
            }
        }

        public IEnumerable<SelectListItem> LanguageSelect
        {
            get
            {
                return Languages.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.LanguageId
                });
            }
        }

        public IEnumerable<SelectListItem> ResidentialStatusSelect
        {
            get
            {
                return ResidentialStatusses.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.ResidentialStatusId
                });
            }
        }

        public IEnumerable<SelectListItem> SocioEconomicStatusSelect
        {
            get
            {
                return SocioEconomicStatusses.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.SociaEconomicStatusId
                });
            }
        }

        public IEnumerable<SelectListItem> PhysicalProvinceSelect
        {
            get
            {
                return Provinces.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.PhysicalProvinceId
                });
            }
        }

        public IEnumerable<SelectListItem> PostalProvinceSelect
        {
            get
            {
                return Provinces.Select(h => new SelectListItem
                {
                    Value = h.Id.ToString(),
                    Text = h.Description,
                    Selected = h.Id == Learner.PostalProvinceId
                });
            }
        }

    }
}