﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WCR_LMS.Models
{
    public class TenderModel
    {
        public TenderContract TenderContract { get; set; }
        public Client Client { get; set; }

    }
}