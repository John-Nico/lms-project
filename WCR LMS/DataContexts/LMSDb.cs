﻿using Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WCR_LMS.DataContexts
{
    public class LMSDb : DbContext
    {

        public LMSDb()
            : base("DefaultConnection")
        {
            Database.SetInitializer<LMSDb>(null);
        }

        //Business Data
        public DbSet<Client> Clients { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Learner> Learners { get; set; }
        public DbSet<Planning> Plannings { get; set; }
        public DbSet<ProvisionalDate> ProvisionalDates { get; set; }
        public DbSet<TenderContract> TenderContract { get; set; }
        public DbSet<RFQContract> RFQContract { get; set; }
        public DbSet<PrivateContract> PrivateContract { get; set; }
        public DbSet<Events> Events { get; set; }
        //Lookups
        public DbSet<AlternateIDTypeLookup> AlternateIDTypes { get; set; }
        public DbSet<DisabilityLookup> Disabilities { get; set; }
        public DbSet<EquityLookup> Equities { get; set; }
        public DbSet<GenderLookup> Genders { get; set; }
        public DbSet<LanguageLookup> Languages { get; set; }
        public DbSet<NationalityLookup> Nationalities { get; set; }
        public DbSet<ProvinceLookup> Provinces { get; set; }
        public DbSet<ResidentialStatusLookup> ResidentialStatusses { get; set; }
        public DbSet<SocioEconomicStatusLookup> SocioEconomicStatusses { get; set; }
        public DbSet<TitleLookup> Titles { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<TrainingRoom> Trainingrooms { get; set; }
        public DbSet<CourseLookup> Courses { get; set; }
    }
}