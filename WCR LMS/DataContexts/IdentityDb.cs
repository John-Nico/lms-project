﻿using Microsoft.AspNet.Identity.EntityFramework;
//using RoboAdvisor.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WCR_LMS.Models;

namespace RoboAdvisor.Web.DataContexts
{
    public class IdentityDb : IdentityDbContext<ApplicationUser>
    {
        public IdentityDb()
            : base("DefaultConnection")
        {
        }

        public static IdentityDb Create()
        {
            return new IdentityDb();
        }
    }
}