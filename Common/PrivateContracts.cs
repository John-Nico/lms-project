﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Common
{
    [Table("PrivateContract")]
    public partial class PrivateContract
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        [DisplayName("Private Name")]
        public string PrivateName { get; set; }
        [DisplayName("Type Of Training")]
        public string TypeOfTraining { get; set; }
        [DisplayName("Private Course Name")]
        public string PrivateCourseName { get; set; }
        [DisplayName("Course Duration")]
        public int CourseDuration { get; set; }
        [DisplayName("SAQA Unit Standard ID")]
        public int SAQAUnitStandardID { get; set; }
        [DisplayName("Non Unit Standard")]
        public int NonUnitStandard { get; set; }
        [DisplayName("Private Contract Description")]
        public string PrivateContractDescription { get; set; }
        [DisplayName("Contract Expire Date")]
        public string ContractExpireDate { get; set; }
        [DisplayName("Price Excl Vat")]
        public int PriceExclVat { get; set; }
        [DisplayName("Last Update Date")]
        public string LastUpdateDate { get; set; }
    }
}
