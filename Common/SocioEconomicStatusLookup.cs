﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("SocioEconomicStatusLookup")]
    public partial class SocioEconomicStatusLookup
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
