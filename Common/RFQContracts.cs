﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Common
{
    [Table("RFQContract")]
    public partial class RFQContract
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        [DisplayName("RFQ Name")]
        public string RFQName { get; set; }
        [DisplayName("RFQ Number")]
        public string RFQNumber { get; set; }
        [DisplayName("Type Of Training")]
        public string TypeOfTraining { get; set; }
        [DisplayName("RFQ Course Name")]
        public string RFQCourseName { get; set; }
        [DisplayName("Course Duration")]
        public int CourseDuration { get; set; }
        [DisplayName("SAQA Unit Standard ID")]
        public int SAQAUnitStandardID { get; set; }
        [DisplayName("Non Unit Standard")]
        public int NonUnitStandard { get; set; }
        [DisplayName("RFQ Contract Description")]
        public string RFQContractDescription { get; set; }
        [DisplayName("Contract Expire Date")]
        public string ContractExpireDate { get; set; }
        [DisplayName("Price Excl Vat")]
        public int PriceExclVat { get; set; }
        [DisplayName("Last Update Date")]
        public string LastUpdateDate { get; set; }
    }
}
