﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("Planning")]
    public partial class Planning
    {
        public int Id { get; set; }
        public int DepartmentId { get; set; }
        public int CourseId { get; set; }
        [DisplayName("Course Type")]
        public string CourseType { get; set; }
        [DisplayName("Number Of Learners")]
        public int NumberOfLearners { get; set; }
        [DisplayName("Planning Name")]
        public string PlanningName { get; set; }
        [DisplayName("Dates")]
        public string Dates { get; set; }
        [DisplayName("Venue Location")]
        public string VenueLocation { get; set; }
        [DisplayName("Group Size")]
        public int GroupSize { get; set; }
        [DisplayName("Status")]
        public string Status { get; set; }
        [DisplayName("Last Update Date")]
        public string LastUpdateDate { get; set; }
    }
}
