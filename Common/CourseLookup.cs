﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("CourseLookup")]
    public partial class CourseLookup
    {
        public int Id { get; set; }
        public string CourseName { get; set; }
    }
}
