﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("Venue")]
    public partial class Venue
    {
        public int Id { get; set; }
        public string VenueName { get; set; }
        public string VenueLocation { get; set; }
    }
}
