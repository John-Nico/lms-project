﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Common
{
    [Table("TenderContract")]
    public partial class TenderContract
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        [DisplayName("Tender Name")]
        public string TenderName { get; set; }
        [DisplayName("Contract Number")]
        public string ContractNumber { get; set; }
        [DisplayName("Type Of Training")]
        public string TypeOfTraining { get; set; }
        [DisplayName("Tender Course Name")]
        public string TenderCourseName { get; set; }
        [DisplayName("Course Duration")]
        public int CourseDuration { get; set; }
        [DisplayName("SAQA Unit Standard ID")]
        public int SAQAUnitStandardID { get; set; }
        [DisplayName("Non Unit Standard")]
        public int NonUnitStandard { get; set; }
        [DisplayName("Tender Contract Description")]
        public string TenderContractDescription { get; set; }
        [DisplayName("Contract Expire Date")]
        public string ContractExpireDate { get; set; }
        [DisplayName("Price Excl Vat")]
        public int PriceExclVat { get; set; }
        [DisplayName("Last Update Date")]
        public string LastUpdateDate { get; set; }
    }
}
