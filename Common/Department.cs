﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("Department")]
    public partial class Department
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        [DisplayName("Department Name")]
        public string DepartmentName { get; set; }
        [DisplayName("Contact Person Name")]
        public string ContactPersonName { get; set; }        
        public string Designation { get; set; }
        [DisplayName("Contact Person Email")]
        public string ContactPersonEmail { get; set; }
        [DisplayName("Contact Person Tel 1")]
        public string ContactPersonTel1 { get; set; }
        [DisplayName("Contact Person Tel 2")]
        public string ContactPersonTel2 { get; set; }
        [DisplayName("Contact Person Fax")]
        public string ContactPersonFax { get; set; }

    }
}
