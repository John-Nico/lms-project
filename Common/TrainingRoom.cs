﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("TrainingRoom")]
    public partial class TrainingRoom
    {
        public int Id { get; set; }
        public string VenueId { get; set; }
        public string TrainingRoomName { get; set; }
        public string Capacity { get; set; }
    }
}
