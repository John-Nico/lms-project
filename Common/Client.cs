﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Common
{
    [Table("Client")]
    public partial class Client
    {
        public int Id { get; set; }
        [DisplayName("Client Name")]
        public string ClientName { get; set; }
        [DisplayName("Vendor Number")]
        public string VendorNumber { get; set; }
        [DisplayName("Vat Number")]
        public string VATNumber { get; set; }
        [DisplayName("Invoice Address")]
        public string InvoiceAddress { get; set; }
        [DisplayName("Physical Address")]
        public string PhysicalAddress { get; set; }
        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }
    }
}
