﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Common
{
    [Table("ProvisionalDate")]
    public partial class ProvisionalDate
    {
        public int Id { get; set; }
        public int PlanningId { get; set; }
        public int AssessorId { get; set; }

        [DisplayName("Group No")]
        public int GroupNo { get; set; }
        [DisplayName("Number Of Learners")]
        public int NumberOfLearners { get; set; }
        [DisplayName("Provisional Date Description")]
        public string ProvisionalDateDescription { get; set; }
        [DisplayName("Dates")]
        public string Dates { get; set; }
        [DisplayName("Colour")]
        public string Colour { get; set; }
        [DisplayName("Status")]
        public string Status { get; set; }
        [DisplayName("Last Update Date")]
        public string LastUpdateDate { get; set; }
    }
}
