﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [Table("Learner")]
    public partial class Learner
    {

        public int Id { get; set; }
        public string AspNetUserId { get; set; }
        public int DepartmentId { get; set; }
        [DisplayName("Learner Number")]
        public int LearnerNumber { get; set; }
        [DisplayName("ID Number")]
        public string IDNumber { get; set; }
        public int AlternateIDTypeId { get; set; }
        [DisplayName("Initials")]
        public string Initials { get; set; }
        public int? TitleId { get; set; }
        [DisplayName("First Name")]
        public string FirstName { get; set; }
        [DisplayName("Last Name")]
        public string LastName { get; set; }        
        [DisplayName("Date of Birth")]
        public string DateOfBirth { get; set; }
        public int? GenderId { get; set; }
        public int? EquityId { get; set; }
        public int? DisabilityId { get; set; }
        public int? LanguageId { get; set; }
        public int? ResidentialStatusId { get; set; }
        public int? SociaEconomicStatusId { get; set; }
        [DisplayName("Telephone")]
        public string TelNumber { get; set; }
        [DisplayName("Cellphone")]
        public string CellNumber { get; set; }
        [DisplayName("Fax")]
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        [DisplayName("Physical Address")]
        public string PhysicalAddress1 { get; set; }
        [DisplayName("Physical Address")]
        public string PhysicalAddress2 { get; set; }
        [DisplayName("Physical Address")]
        public string PhysicalAddress3 { get; set; }
        [DisplayName("Physical Postal Code")]
        public int? PhysicalPostalCode { get; set; }
        public int? PhysicalProvinceId { get; set; }
        [DisplayName("Postal Address")]
        public string PostalAddress1 { get; set; }
        [DisplayName("Postal Address")]
        public string PostalAddress2 { get; set; }
        [DisplayName("Postal Address")]
        public string PostalAddress3 { get; set; }
        [DisplayName("Postal Code")]
        public int? PostalCode { get; set; }
        public int? PostalProvinceId { get; set; }
        [DisplayName("Employer SDL Number")]
        public string EmployerSDLNumber { get; set; }
        
    }
}
